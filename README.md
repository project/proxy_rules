# Proxy Rules

This module can be used to write Drupal rules to take actions on
unwanted connections, e.g. through anonymous proxies or from bots.

Unlike the related block_proxies module, proxy_rules does not automatically
block all requests on its own, but requires the site builder to define his
or her own logic. Possible uses include writing a hook_boot to block all unwanted
incoming connections, or use of the rules module to limit access to a subset 
of site functionality, e.g. account creation.

When used with the Drupal rules module, proxy_rules adds several new conditions:

* "Remote IP is on a DNSBL" checks whether the remote IP exists in one or more DNS blacklists, like dnsbl.spfbl.net. 
* "Remote hostname contains substring" checks the remote Hostname for a pattern, and can be used to identify search crawlers.
* "Remote IP is a Google Proxy" checks whether the connection is made through a Google Proxy.
* "Remote IP is a Tor Exit Node" checks whether the connection is made from Tor.
* "Remote IP address or subnet match" checks the remote IP against a list of addresses or CIDR subnets.

A typical example for this is a rule that blocks new user accounts if they were created from a certaian subnet, 
through a known anonymous proxy, or a Tor exit node.

## Requirements

* The Drupal rules module is recommended, but all provided functionality can also be called from custom modules.
* DNSBL checks require an external DNSBL library like PEAR's Net_DNSBL.
* To use the Tor Exit Node check, `composer require dapphp/torutils`
* To use the IP2Location proxy checks, `composer require ip2location/ip2proxy-php`
* To use the Udger proxy checks, `composer require udger/udger-php`
