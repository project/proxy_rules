<?php

/**
 * @file
 * Provides rules integration for the Proxy Rules module.
 *
 * @author Enno Rehling <https://www.drupal.org/user/3465851>
 * @link https://www.servebolt.com/
 */

/**
 * Implements hook_rules_condition_info().
 */
function proxy_rules_rules_condition_info() {
  $conditions = array(
    'proxy_rules_condition_is_google_proxy' => array(
      'group' => t('Proxy Rules'),
      'label' => t('Remote IP is a Google Proxy'),
      'parameter' => array(),
    ),
    'proxy_rules_condition_is_tor_proxy' => array(
      'group' => t('Proxy Rules'),
      'label' => t('Remote IP is a Tor Exit Node'),
      'parameter' => array(),
    ),
    'proxy_rules_condition_ip_matches' => array(
      'group' => t('Proxy Rules'),
      'label' => t('Remote IP address or subnet match'),
      'parameter' => array(
        'cidr_list' => array(
          'type' => 'text',
          'label' => 'One or more address or subnet',
        ),
      ),
    ),
    'proxy_rules_condition_hostname_matches' => array(
      'group' => t('Proxy Rules'),
      'label' => t('Remote hostname contains substring'),
      'parameter' => array(
        'patterns' => array(
          'type' => 'text',
          'label' => 'One or more substrings',
        ),
      ),
    ),
    'proxy_rules_condition_is_listed' => array(
      'group' => t('Proxy Rules'),
      'label' => t('Remote IP is on a DNSBL'),
      'parameter' => array(
        'proxy_rules_list' => array(
          'type' => 'text',
          'label' => 'One or more DNSBL',
        ),
      ),
    ),
    'proxy_rules_condition_is_proxy' => array(
      'group' => t('Proxy Rules'),
      'label' => t('Remote IP is a proxy'),
      'parameter' => array(
        'service' => array(
          'type' => 'text',
          'label' => 'Proxy Service',
          'default value' => 'any',
          'options list' => '_proxy_rules_proxy_service_options',
        ),
      ),
    ),
  );
  return $conditions;
}

/**
 * Populate proxy detection service option list.
 *
 * @return array
 *   A list of available proxy services.
 */
function _proxy_rules_proxy_service_options() {
  $services = array(
    'tor' => t('tor exit node'),
    'udger_db' => t('local database from udger.com'),
    'ip2p_db' => t('local database from ip2location.com'),
    'udger_cloud' => t('udger.com cloud API'),
    'ip2p_cloud' => t('ip2location.com cloud API'),
  );
  $available = proxy_rules_available_services();
  return array_filter($services, function ($key) use ($available) {
    return in_array($key, $available);
  }, ARRAY_FILTER_USE_KEY);
}

/**
 * Use one of the implemented methods to check the remote IP.
 *
 * @return bool
 *   TRUE iff the remote IP is detected as a proxy.
 */
function proxy_rules_condition_is_proxy($service) {
  $ip_addr = ip_address();
  try {
    switch ($service) {
      case 'ip2p_db':
        $result = proxy_rules_ip2proxy_db_get_all($ip_addr);
        if ($result) {
          return $result['isProxy'] != 0;
        }
        break;

      case 'ip2p_cloud':
        $result = proxy_rules_ip2proxy_db_get_all($ip_addr);
        if ($result) {
          return $result['isProxy'] != 'NO';
        }
        break;

      case 'udger_db':
        $result = proxy_rules_udger_parse_db($ip_addr);
        if ($result) {
          $code = $result['ip_address']['ip_classification_code'];
          return 'unrecognized' != $code;
        }
        break;

      case 'udger_cloud':
        $result = proxy_rules_udger_parse_cloud($ip_addr);
        if ($result) {
          $code = $result['ip_address']['ip_classification_code'];
          return 'unrecognized' != $code;
        }
        break;

      case 'tor':
        return proxy_rules_is_tor_proxy($ip_addr);

      default:
        break;
    }
  }
  catch (Exception $ex) {
    watchdog('proxy_rules', "!service check of !ip failed: !error.", array(
      '!service' => $service,
      '!ip' => $ip_addr,
      '!error' => $ex->getMessage(),
    ), WATCHDOG_ERROR);
  }
  return FALSE;
}

/**
 * Determine whether the remote host is a Google proxy.
 *
 * @return bool
 *   TRUE if the remote host is a Google proxy.
 */
function proxy_rules_condition_is_google_proxy() {
  $ip_addr = ip_address();
  return proxy_rules_is_google_proxy($ip_addr);
}

/**
 * Simple DNS test for Tor proxies.
 *
 * @return bool
 *   TRUE if the remote host is a Tor exit node.
 */
function proxy_rules_condition_is_tor_proxy() {
  $ip_addr = ip_address();
  return proxy_rules_is_tor_proxy($ip_addr);
}

/**
 * Determine whether the remote ip matches a list of IP subnets.
 *
 * @param string $cidr_list
 *   A newline-delimited list of CIDR subnets or IP adresses.
 *
 * @return bool
 *   TRUE is remote IP matches any of the subnets.
 */
function proxy_rules_condition_ip_matches($cidr_list) {
  $ip_addr = ip_address();
  $cidrs = explode('\n', str_replace(' ', '', $cidr_list));
  foreach ($cidrs as $cidr) {
    $cidr = filter_var($cidr, FILTER_VALIDATE_REGEXP, array(
      'options' => array(
        'regexp' => '/[\.:a-z0-9]+\/\d+/',
      ),
    ));
    if ($cidr !== FALSE && proxy_rules_cidr_match($ip_addr, $cidr)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Determine whether the remote host's name contains a substring.
 *
 * @param string $patterns
 *   A newline-delimited list of strings.
 *
 * @return bool
 *   TRUE if any of the substrings are contained in the remote host's name.
 */
function proxy_rules_condition_hostname_matches($patterns) {
  $ip_addr = ip_address();
  $hostname = proxy_rules_get_hostname($ip_addr);
  $strings = explode('\n', str_replace(' ', '', $patterns));
  return proxy_rules_hostname_matches($hostname, $strings);
}

/**
 * Check remote IP against one or more DNS blacklists (DNSBL).
 *
 * @param string $proxy_rules_list
 *   A newline-delimited list of DNSBLs.
 *
 * @return bool
 *   TRUE is a match was found on any of the DNSBLs.
 */
function proxy_rules_condition_is_listed($proxy_rules_list) {
  $ip_addr = ip_address();
  $dnsbls = explode('\n', str_replace(' ', '', $proxy_rules_list));
  return proxy_rules_is_listed($ip_addr, $dnsbls);
}
